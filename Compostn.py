#program on car details to demonstrate Composition

class car_details:

     def __init__(self,color,type):
         self.color=color
         self.type=type

     def details(self):
         return self.color,self.type

class Car:

    def __init__(self,name,color,type):
        self.name=name
        self.d=car_details(color,type)

    def Car_d(self):
        return self.name,self.d.details()

C=Car("Toyota","Red","A")
print ("Details of the car"+(C.Car_d()))