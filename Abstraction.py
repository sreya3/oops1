# program to print the no of sides and shape to demonstrate abstract method

from abc import ABC

class shape(ABC):

    def model(self):
        pass


class Rectangle(shape):

    def model(self):
        print("I am Rrectangle and I have 4 sides")


class Triangle(shape):

    def model(self):
        print("I am Triangle and I have 3 sides")


R = Rectangle()
R.model()

T= Triangle()
T.model()