#program to print name to demostrate Encapsulation

class Person():
   def __init__(self):
      self.name = "Rahul"

   def getname(self):
       print (self.name)

   def setname(self, name):
      self.name = name

P = Person()

P.getname()
P.setname("Sunny")
print(P.name)
P.setname("Ram")
print(P.name)
